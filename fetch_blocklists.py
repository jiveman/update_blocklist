#!/usr/bin/env python
# Blocklist from https://www.iblocklist.com/lists

import zipfile
from io import BytesIO
from configparser import ConfigParser
import urllib
from urllib.parse import urlparse, urlencode

import re
import os
# import ipdb

import requests
# cache using redis
# import redis
# cache = redis.Redis(host='localhost', port=6379, db=0)
# using diskcache
from diskcache import Cache
cache = Cache()
# cache using ram
# from cachetools import cached, TTLCache  # 1 - let's import the "cached" decorator and the "TTLCache" object from cachetools
# cache = TTLCache(maxsize=10, ttl=60 * 60 * 4)  # 2 - let's create the cache object.

# expire in seconds
expire = 60 * 60 * 23





def merge_dicts(dict1, dict2):
    return {k: v for d in [dict1, dict2] for k, v in d.items()}


def tuples_to_dict(pairs):
    return dict((x, y) for x, y in pairs)


secrets_config = ConfigParser()
secrets_config.read('secrets.ini')

lists_config = ConfigParser()
lists_config.read('lists.ini')

def feeds():

    feeds = list()
    for title in lists_config.sections():
        params = tuples_to_dict(lists_config[title].items())
        base_url = params['base_url']
        base_domain = urlparse(base_url).netloc
        additional_params = {}
        try:
            if secrets_config[base_domain]:
                additional_params = tuples_to_dict(secrets_config[base_domain].items())
            if additional_params:
                params = merge_dicts(params, additional_params)
        except KeyError as err:
            print("no secrets.ini key values found for domain: {}".format(base_domain))
        del params['base_url']
        urlencoded = urlencode(params)
        # feeds.append((title, "{}/?{}".format(base_url, urlencoded)))
        yield({'name': title, 'url': "{}/?{}".format(base_url, urlencoded)})
    # return feeds

def actions():
    actions_config = ConfigParser()
    actions_config.read('actions.ini')
    actions = list()
    for title in actions_config.sections():
        actions.append([x[1] for x in actions_config[title]])
        for k,v in actions_config[title].items():
            yield({'name': title, 'cmd': [v]})


# @cached(cache)
def fetch_feed(url):
    if cache.get(url):
        print("Fetched from cache")
        return cache.get(url)
    r = requests.get(url)
    z = zipfile.ZipFile(BytesIO(r.content))
    z.extractall()
    f = open(re.findall(r'list=(\S{1,25})&', url)[0] + ".txt", "r")
    ip_list = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2})', f.read())
    print("Caching for {} seconds".format(expire))
    cache.set(url, ip_list, expire)
    return ip_list

def write_list(name, ips):
    if not name:
        raise KeyError('Requires UDL Name')
    if not ips or len(ips) <= 10:
        raise KeyError('Minimum IP threshold not met (10 IPs)')
    filename = ('./{}_list.csv'.format(name))
    fh = open(filename, 'w', encoding="utf-8")
    for ip in ips:
        # fh.write("{},{}\n".format(name,ip))
        fh.write("{},{} list\n".format(ip,name))
    fh.close()
    print('wrote: %s' % (filename))
    return filename

if __name__ == '__main__':
    for feed in feeds():
        # print('stop here... '); import sys; sys.exit()
        print('Processing {} ({})'.format(feed['name'], feed['url']))
        ips = fetch_feed(feed['url'])
        source_file = write_list(feed['name'], ips)
        for action in actions():
            print("running {}".format(action['name']))
            cmd = action['cmd']
            if action['name'] == 'update_udl':
                cmd.append('--list {}'.format(feed['name']))
                cmd.append('--source {}'.format(source_file))
            print("Running: {}".format(' '.join(cmd)))
            # ipdb.set_trace()
            os.system(' '.join(cmd))
